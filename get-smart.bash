#!/bin/bash

# Wrapper script to be used as a `credential.helper` for `git`:
#
#     git config credential.helper '!get-smart $@'
#
# Calls `smart-oven`, selects the menu option given by the
# `SMART_OVEN_MENU_CHOICE` environment variable, reads the environment
# variables which `smart-oven` prints, then uses `env` to feed them to
# `aws codecommit credential-helper`.
#
# Version 0.2: writes/reads credentials to/from a file in the user's
# home directory, and only calls `smart-oven` again if the credentials
# are older than one hour.

if [[ ! -v SMART_OVEN_MENU_CHOICE ]] ; then
    echo 'environment variable `SMART_OVEN_MENU_CHOICE` is not set' >&2
    exit 2
fi

if (( $# == 0 )) ; then
    echo 'expected at least one command-line argument' >&2
    exit 2
fi

echo 'get-smart: invoked with command-line arguments:' '`'"$*"'`' >&2

declare -r AWS_CREDENTIALS_FILE="${HOME}/.aws-credentials"
declare -ir AWS_CREDENTIALS_EXPIRY=3600
declare -a AWS_CREDENTIALS

if [[ ! -e "${AWS_CREDENTIALS_FILE}" ]] || (( $(date '+%s') - $(stat -c '%Y' "${AWS_CREDENTIALS_FILE}") > ${AWS_CREDENTIALS_EXPIRY}  )) ; then
    echo 'get-smart: calling `smart-oven`, selecting option '"${SMART_OVEN_MENU_CHOICE}"', and writing credentials to `'"${AWS_CREDENTIALS_FILE}"'`' >&2
    echo "${SMART_OVEN_MENU_CHOICE}" | smart-oven 2>/dev/null | sed -n 's/^set //p' > "${AWS_CREDENTIALS_FILE}" || exit
fi

echo 'get-smart: reading AWS credentials from `'"${AWS_CREDENTIALS_FILE}"'`' >&2
IFS=$'\n' read -r -d '' -a AWS_CREDENTIALS < "${AWS_CREDENTIALS_FILE}"
# echo 'get-smart:' "${AWS_CREDENTIALS[@]}" >&2
echo 'get-smart: passing credentials to `aws codecommit credential-helper`' >&2
# echo 'get-smart: command: env '"${AWS_CREDENTIALS[@]}"' aws codecommit credential-helper '"$@" >&2
exec env "${AWS_CREDENTIALS[@]}" aws codecommit credential-helper "$@"

# End of file
